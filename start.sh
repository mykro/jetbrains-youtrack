#!/usr/bin/env bash
docker run -d \
   -p 10401:8080 \
   -v /jetbrains/youtrack/:/jetbrains/youtrack/ \
   -v /jetbrains/youtrack/backup:/jetbrains/youtrack/backup \
   -v /jetbrains/youtrack/data:/jetbrains/youtrack/data \
   -v /jetbrains/youtrack/logs:/jetbrains/youtrack/logs \
   -v /jetbrains/youtrack/tmp:/jetbrains/youtrack/tmp \
   --name=jb-yt-001 \
   --restart=always \
   mykro/jetbrains-youtrack
