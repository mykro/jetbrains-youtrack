# mykro/jetbrains-youtrack
A build on top of Debian 8
For information on Youtrack check out: https://www.jetbrains.com/youtrack/

## Image Sizes
[![](https://badge.imagelayers.io/mykro/jetbrains-youtrack:latest.svg)](https://imagelayers.io/?images=mykro/jetbrains-youtrack:latest) 6.5.16981, latest

## Docker Switches
### -p --port
This image runs on port 8080 and assumes being linked into a loadbalancer or reverse proxy such as Nginx. To ensure porting out the correct port use -p HOST_PORT:DOCKER_PORT.
```sh
$ docker run -d -p 10080:8080 mykro/jetbrains-youtrack
```

### -v --volumes
To enable data persistence, make sure all folders created have been chown'ed to docker:docker.

#### Root home folder
```sh
$ docker run -d -v /HOST/FOLDER/:/jetbrains/youtrack/ mykro/jetbrains-youtrack
```

#### Backup folder mapping
```sh
$ docker run -d -v /HOST/FOLDER/backup:/jetbrains/youtrack/backup mykro/jetbrains-youtrack
```

#### Data Folder
```sh
$ docker run -d -v /HOST/FOLDER/data:/jetbrains/youtrack/data mykro/jetbrains-youtrack
```

#### Log Folder: 
```sh
$ docker run -d -v /HOST/FOLDER/logs:/jetbrains/youtrack/logs mykro/jetbrains-youtrack
```

#### Temp Folder 
```sh
$ docker run -d -v /jetbrains/youtrack/tmp:/jetbrains/youtrack/tmp mykro/jetbrains-youtrack
```

### --name
This is a nice switch which allows you to name an instance. This is handy if you're running High Availability with multiple containers mapped to the same host folders. This way it's easier to define the different containers under docker stats, and start/stop commands. 
For example:
```sh
$ docker run -d --name=jbyt-001 -p 8081:8080 mykro/jetbrains-youtrack
$ docker run -d --name=jbyt-002 -p 8082:8080 mykro/jetbrains-youtrack
$ docker stop jbyt-001
$ docker start jbyt-001
$ docker stats jbyt-001 jbyt-002
```

### --restart
If in event of container failure, you can get docker to auto-restart by using --restart=always
For example:
```sh
$ docker run -d --restart=always mykro/jetbrains-youtrack
```

## Running Images
So, putting it all together you'll end up with a command as follows. As mentioned above, ensure that docker has permissions on the host folders otherwise it will fail.
```sh
$ docker run -d \
   -p 10401:8080 \
   -v /HOST/FOLDER/:/jetbrains/youtrack/ \
   -v /HOST/FOLDER/backup:/jetbrains/youtrack/backup \
   -v /HOST/FOLDER/data:/jetbrains/youtrack/data \
   -v /HOST/FOLDER/logs:/jetbrains/youtrack/logs \
   -v /HOST/FOLDER/tmp:/jetbrains/youtrack/tmp \
   --name=jbyt-001 \
   --restart=always \
   mykro/jetbrains-youtrack
```

## Building a custom image
This image depends on the Server runtime version of Java I have built in mykro/java8-jre. This can be pulled with
```sh
$ docker pull mykro/java8-jre:latest
```
Make your edits under the build file, namely the location of files within the container. I have added an Environment Variable that you can change to specify how much RAM you are allowing Java to consume when running. Currently, it is set in the build file as 1024MBs.

## Pulling different versions
Pull a different image with: 
```sh
$ docker pull mykro/jetbrains-youtrack:[latest|6.5.16981]
```
To see more versions, check the image tag tab above!

## Bugs/Issues/Updates
If you've found a bug/issue, you can find the repo @ https://bitbucket.org/mykro/jetbrains-youtrack/overview